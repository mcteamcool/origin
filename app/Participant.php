<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
  protected $table = 'participant';

  protected $fillable = [
    'name',
  ];

  protected $hidden = [];

  public function event() {
    return $this->belongsTo('App\Event');
  }
}
