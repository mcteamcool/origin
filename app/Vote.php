<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
  protected $table = 'vote';

  protected $fillable = [
    'id', 'name',
  ];

  protected $hidden = ['created_at', 'updated_at', 'event_id'];

  public function event() {
    return $this->belongsTo('App\Event');
  }

  public function opinions() {
    return $this->hasMany('App\Opinion');
  }
}
