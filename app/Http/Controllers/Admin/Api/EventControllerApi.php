<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Event;
use App\Http\Resources\EventResource;
use App\Http\Resources\EventsResource;
use App\Http\Controllers\Admin\AdminController as Admin;

class EventControllerApi extends Controller
{
  // Display on admin
  public function index(){
    return view('/admin/api/event');
  }

  // Display the specified Event
  public function show($id)
  {
    if ( Event::where('id', $id)->exists() ) {
      EventResource::withoutWrapping();
      return new EventResource(Event::find($id));

      // return new EventsResource(Event::find($id));
    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => "That object does not exist"
      ]);
    }
  }

  // Get owner of event
  static function getNumberOfParticipant($eventId)
  {
    $numberOfParticipant = DB::table('participant')
    ->where('event_id', $eventId)
    ->count();

    return $numberOfParticipant;
  }

  // Create new event
  public function store(Request $request)
  {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Generate link
      $link = md5(microtime(true));

      // Query
      try
      {
        $eventId = DB::table('event')->insertGetId([
          'name' => $request->name,
          'note' => $request->note,
          'canCreateVoteFromOthers' => $request->canCreateVoteFromOthers,
          'link' => $link
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Create event successfully",
          'link' => $link
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }
    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }

  // Update event
  public function update(Request $request, $id)
  {
      // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);
    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('event')
        ->where('id', $id)
        ->update([
          'name' => $request->name,
          'note' => $request->note,
          'canCreateVoteFromOthers' => $request->canCreateVoteFromOthers,
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Update event successfully"
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }

    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }
}
