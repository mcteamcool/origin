<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Admin\AdminController as Admin;

class ParticipantControllerApi extends Controller
{
  // Display on admin
  public function index(){
    return view('/admin/api/participant');
  }

  // Create new participant
  public function store(Request $request) {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('participant')->insert([
          'name' => $request->name,
          'event_id' => $request->event_id
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Create participant successfully",
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }
    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }

  // Update participant
  public function update(Request $request, $id)
  {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('participant')
        ->where('id', $id)
        ->update([
          'name' => $request->name
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Update participant successfully"
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }

    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }
}
