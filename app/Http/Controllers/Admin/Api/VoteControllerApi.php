<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Vote;
use App\Http\Resources\VoteResource;
use App\Http\Controllers\Admin\AdminController as Admin;

class VoteControllerApi extends Controller
{
  // Display on admin
  public function index(){
    return view('/admin/api/vote');
  }

  // Display the specified Vote
  public function show($id)
  {
    if ( Vote::where('id', $id)->exists() ) {
      return new VoteResource(Vote::find($id));
    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => "That object does not exist"
      ]);
    }
  }

  // Create Vote
  public function store(Request $request) {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('vote')->insert([
          'name' => $request->name,
          'event_id' => $request->event_id
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Create vote successfully",
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }
    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }

  // Update Vote
  public function update(Request $request, $id)
  {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('vote')
        ->where('id', $id)
        ->update([
          'name' => $request->name
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Update vote successfully"
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }

    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }

}
