<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Date_Par;
use App\Http\Resources\Date_ParResource;
use App\Http\Controllers\Admin\AdminController as Admin;

class Date_ParControllerApi extends Controller
{
  // Display on admin
  public function index(){
    return view('/admin/api/date_par');
  }

  // Create Date_Par
  public function store(Request $request) {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('date_par')->insert([
          'isGoing' => $request->isGoing,
          'isConsidering' => $request->isConsidering,
          'date_id' => $request->date_id,
          'participant_id' => $request->participant_id
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Create date_par successfully",
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }
    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }

  // Update Date_Par
  public function update(Request $request, $id)
  {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('date_par')
        ->where('id', $id)
        ->update([
          'isGoing' => $request->isGoing,
          'isConsidering' => $request->isConsidering
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Update date_par successfully"
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }

    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }
}
