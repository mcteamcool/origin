<?php

namespace App\Http\Controllers\Admin\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Resources\UserResource;

class UserControllerApi extends Controller
{
  // Get all users
  public function index()
  {
    return new UserResource(User::find(1));
  }
}
