<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Admin\AdminController as Admin;

class OpinionControllerApi extends Controller
{
  // Display on admin
  public function index(){
    return view('/admin/api/opinion');
  }

  // Create Vote
  public function store(Request $request) {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('opinion')->insert([
          'name' => $request->name,
          'vote_id' => $request->vote_id
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Create opinion successfully",
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }
    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }

  // Update Vote
  public function update(Request $request, $id)
  {
    // Check vaild request
    $isVaildRequest = Admin::isVaildRequest($request);

    if ($isVaildRequest)
    {
      // Query
      try
      {
        DB::table('opinion')
        ->where('id', $id)
        ->update([
          'name' => $request->name,
          'voted' => $request->voted
        ]);

        return response()->json([
          'status' => true,
          'msg' => "Update opinion successfully"
        ]);
      }
      catch(QueryException $ex)
      {
        return response()->json([
          'status' => false,
          'error' => $ex
        ]);
      }

    }
    else
    {
      return response()->json([
        'status' => false,
        'msg' => 'Request is invaild.'
      ]);
    }
  }
}
