<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AdminLoginRequest;

class AdminLoginController extends Controller
{
  // Get Login
  public function getLoginAdmin() {
    if (Auth::check()) {
      return redirect('admincp');
    } else {
      return view('admin.auth.login');
    }
  }

  public function postLoginAdmin(AdminLoginRequest $request)
  {
    $login = [
      'email' => $request->txtEmail,
      'password' => $request->txtPassword
    ];

    if (Auth::attempt($login) && Auth::user()->isAdmin())
    {
      return redirect('admincp');
    }
    else
    {
      return redirect()->back()->with('status', 'Email hoặc Password không chính xác')->withInput();
    }
  }

  public function getLogoutAdmin()
  {
    Auth::logout();
    return redirect()->route('getLoginAdmin');
  }
}