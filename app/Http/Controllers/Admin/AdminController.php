<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Api\UserControllerApi;
use App\Http\Controllers\Admin\Api\EventControllerApi;

class AdminController extends Controller
{
  function index()
  {
    return view('/admin/admin');
  }

  static function isVaildRequest($request)
  {
    $app_key = config('app.apiKey');
    $req_key = ($request->key) ? $request->key : null;

    if ( !isset($req_key) || md5($req_key) != $app_key )
    {
      return false;
    }
    else
    {
      return true;
    }
  }
}
