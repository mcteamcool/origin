<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Controllers\Admin\Api\EventControllerApi as EventApi;

class EventResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'note' => $this->note,
            'link' => $this->link,
            'canCreateVoteFromOthers' => $this->canCreateVoteFromOthers,
            'dates' => new DatesResource($this->dates),
            'votes' => new VotesResource($this->votes),
            'numberOfParticipant' => EventApi::getNumberOfParticipant($this->id)
        ];
    }

}