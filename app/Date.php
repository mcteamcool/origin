<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
  protected $table = 'date';

  protected $fillable = [
    'name',
  ];

  protected $hidden = [];

  public function event() {
    return $this->belongsTo('App\Event');
  }

  public function date_pars()
  {
      return $this->hasMany('App\Date_Par');
  }
}
