<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
  protected $table = 'opinion';

  protected $fillable = [
    'name', 'voted'
  ];

  protected $hidden = ['created_at', 'updated_at', 'vote_id'];

  public function vote() {
    return $this->belongsTo('App\Vote');
  }
}
