<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $table = 'event';

  protected $fillable = [
    'name', 'note', 'link', 'canCreateVoteFromOthers',
  ];

  protected $hidden = ['created_at', 'updated_at'];

  public function user() {
    return $this->belongsTo('App\User');
  }

  public function participants() {
    return $this->hasMany('App\Participant');
  }

  public function dates()
  {
      return $this->hasMany('App\Date');
  }

  public function votes()
  {
      return $this->hasMany('App\Vote');
  }
}