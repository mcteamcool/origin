<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;

  protected $fillable = [
    'name', 'email', 'password', 'ipaddress',
];

protected $hidden = [
    'password', 'remember_token', 'created_at', 'updated_at'
];

    // Check Admin Role
public function isAdmin()
{
    if ($this->id !== 1)
    {
        return false;
    }

    return true;
}

public function events()
{
    return $this->hasMany('App\Event');
}
}
