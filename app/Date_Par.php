<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date_Par extends Model
{
  protected $table = 'date_par';

  protected $fillable = [
    'isGoing', 'isConsidering', 'comment',
  ];

  protected $hidden = ['created_at', 'updated_at'];
}
