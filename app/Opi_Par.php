<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opi_Par extends Model
{
  protected $table = 'opi_par';

  protected $fillable = [];

  protected $hidden = [];
}
