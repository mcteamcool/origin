# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Getting Started ###
* composer install
* php artisan key:generate

### Running Seeders ###

* composer dump-autoload
* php artisan db:seed --class=UsersTableSeeder

### Rollback and refresh migrations ###
* php artisan migrate:refresh --seed