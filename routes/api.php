<?php

use Illuminate\Http\Request;

// Api
Route::resource('user', 'UserControllerApi', ['only' => [
    'index', 'show'
]]);

Route::resource('event', 'EventControllerApi', ['only' => [
    'index', 'show', 'store', 'update'
]]);

Route::resource('vote', 'VoteControllerApi', ['only' => [
    'show', 'store', 'update'
]]);

Route::resource('opinion', 'OpinionControllerApi', ['only' => [
    'store', 'update'
]]);

Route::resource('participant', 'ParticipantControllerApi', ['only' => [
    'store', 'update'
]]);

Route::resource('date', 'DateControllerApi', ['only' => [
    'store', 'update', 'delete'
]]);

Route::resource('date_par', 'Date_ParControllerApi', ['only' => [
    'store', 'update'
]]);