<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// Admin
Route::get('admincp/login', ['as' => 'getLoginAdmin', 'uses' => 'Admin\Auth\AdminLoginController@getLoginAdmin']);
Route::post('admincp/login', ['as' => 'postLoginAdmin', 'uses' => 'Admin\Auth\AdminLoginController@postLoginAdmin']);
Route::get('admincp/logout', ['as' => 'getLogoutAdmin', 'uses' => 'Admin\Auth\AdminLoginController@getLogoutAdmin']);

Route::group(['middleware' => 'is-admin', 'prefix' => 'admincp'], function () {
  Route::get('/', ['as' => 'admincp', 'uses' => 'Admin\AdminController@index']);

  Route::get('api/event', 'Admin\Api\EventControllerApi@index');
  Route::get('api/vote', 'Admin\Api\VoteControllerApi@index');
  Route::get('api/opinion', 'Admin\Api\OpinionControllerApi@index');
  Route::get('api/participant', 'Admin\Api\ParticipantControllerApi@index');
  Route::get('api/date', 'Admin\Api\DateControllerApi@index');
  Route::get('api/date_par', 'Admin\Api\Date_ParControllerApi@index');
});