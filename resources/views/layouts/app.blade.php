<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Semantic-ui -->
  <link href="{{ asset('css/semantic.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/calendar.min.css') }}" rel="stylesheet">
</head>
<body>
  <div id="wrap">
    <!-- Header -->
    <div id="header">
        <!-- Logo -->
        <div class="logo">
            <img src="{{ asset('img/dummy-logo.png') }}" alt="MC Event">
        </div>
        <!-- Menu -->
        <div class="menu-top">
            <div class="ui secondary menu green">
                <a class="item active">Trang chủ</a>
                <a class="item">
                    Hướng dẫn
                </a>
                <a class="item">
                    Giới thiệu
                </a>
                <div class="right menu">
                    <!-- Buttons Authen -->
                    <div class="btns">
                        <button class="ui inverted green button">Đăng nhập</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner -->
    <div id="banner">
        <img class="ui fluid image" src="http://onlinecreative.org/wp-content/plugins/pushdownbanner/images/image-example.jpg">
    </div>

    <!-- Create Event -->
    <div id="createEvent">
        <h2 class="ui center aligned icon header">
            <i class="circular add to calendar icon"></i>
            Tạo sự kiện hoàn toàn miễn phí chỉ với 3 bước đơn giản
        </h2>
        <form class="ui form">
            <div class="ui horizontal segments">
                <div class="ui segment form_segment1">
                    <div class="ui raised segment">
                        <a class="ui red ribbon label">Bước 1</a>
                        <span>Nhập thông tin sự kiện</span>
                    </div>
                    <!-- Event Name -->
                    <div class="field">
                        <label>Tên sự kiện</label>
                        <div class="">
                            <div class="field">
                                <input type="text" name="shipping[first-name]" placeholder="Tên sự kiện">
                            </div>
                        </div>
                    </div>
                    <!-- Event Note -->
                    <div class="field">
                        <label>Ghi chú</label>
                        <textarea></textarea>
                    </div>
                </div>
                <div class="ui segment form_segment2">
                    <div class="ui raised segment">
                        <a class="ui green ribbon label">Bước 2</a>
                        <span>Chọn ngày bắt đầu sự kiện <br> 
                            <i class="asterisk icon"></i>
                            <span>Bạn có thể chọn nhiều lịch để các thành viên đưa ra biểu quyết</span>
                        </span>
                    </div>
                    <div class="ui calendar" id="example1">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="Nhấp vào đây để thêm lịch mới">
                        </div>
                        <div class="ui icon button" data-tooltip="Xóa lịch này">
                            <i class="remove icon"></i>
                        </div>
                    </div>
                    <div class="ui calendar" id="example1">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="Ngày/Giờ">
                        </div>
                    </div>
                </div>
                <div class="ui segment form_segment3">
                    <div class="ui raised segment">
                        <a class="ui blue ribbon label">Bước 3</a>
                        <span>Tùy chỉnh</span><br>
                        <i class="asterisk icon"></i>
                        <span>Bạn có thể bỏ qua bước này và ấn nút Tạo sự kiện</span>
                    </div>

                    <!-- Calendar Note -->
                    <div class="field">
                        <div class="form_action">
                            <div class="ui card">
                                <div class="content">
                                    <div class="ui mini icon input">
                                        <input type="text" placeholder="Đặt câu hỏi">
                                    </div>
                                </div>
                                <div class="extra content">
                                    <div class="vote">
                                        <div class="ui checkbox">
                                            <input type="checkbox" name="vote">
                                            <label>
                                                <div class="ui mini icon input">
                                                    <input type="text" placeholder="Đặt câu hỏi">
                                                </div>
                                            </label>
                                        </div>
                                        <div class="ui icon button seg3_del" data-tooltip="Xóa lịch này">
                                            <i class="remove icon"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="extra content">
                                    <button class="mini ui button">Tạo cuộc thăm dò ý kiến</button>
                                </div>
                            </div>
                            <button class="ui green button createBtn">Tạo sự kiện</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- Recently Event -->
    <div id="recentlyEvent"></div>

    <!-- Footer -->
    <div id="footer"></div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/semantic.min.js') }}"></script>
<script src="{{ asset('js/calendar.min.js') }}"></script>
<script language='javascript'>
    $(document).ready(function(){
        $('.ui.accordion').accordion();
    });
    $('#example1').calendar();
</script>
</body>
</html>
