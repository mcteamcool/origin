<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>MC-Event AdminCP</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Semantic-ui -->
  <link href="{{ asset('css/semantic.min.css') }}" rel="stylesheet">
</head>
<body>
  <div class="admin-login">
    <div class="ui centered grid container">
      <div class="nine wide column">

            <!-- Validation errors -->
            @if (count($errors) >0)
              <div class="ui icon warning message">
                <i class="lock icon"></i>
                <div class="content">
                  <ul>
                    @foreach($errors->all() as $error)
                      <li class="text-danger"> {{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              </div>
            @elseif(session('status'))
              <!-- Login failed error -->
              <div class="ui icon warning message">
                <i class="lock icon"></i>
                <div class="content">
                  <div class="header">
                    Login failed!
                  </div>
                  <p>{{ session('status') }}</p>
                </div>
              </div>
            @else
              <div class="ui icon green message">
                <i class="icon user circle outline"></i>
                <div class="content">
                  <div class="header">
                    MC-Event Control Pane
                  </div>
                  <p>{{ session('status') }}</p>
                </div>
              </div>
            @endif

        <div class="ui fluid card">
          <div class="content">
            <form class="ui form" method="POST" action="{{ route('postLoginAdmin') }}">
              {{ csrf_field() }}
              <div class="field">
                <label>User</label>
                <input type="text" name="txtEmail" placeholder="Email" value="{{ old('txtEmail') }}" required autofocus>
              </div>
              <div class="field">
                <label>Password</label>
                <input type="password" name="txtPassword" placeholder="Password" value="{{ old('txtPassword') }}" required autofocus>
              </div>
              <button class="ui primary labeled icon button" type="submit">
                <i class="unlock alternate icon"></i>
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('js/semantic.min.js') }}"></script>
  <script language='javascript'>
   $(document).ready(function(){
    $('.ui.accordion').accordion();
  });
</script>
</body>
</html>