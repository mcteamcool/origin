<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title')</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Semantic-ui -->
  <link href="{{ asset('css/semantic.min.css') }}" rel="stylesheet">
</head>
<body>
  <!-- Content -->
  <div class="admin ui">

    <div class="admin-vertical-nav">
      <div class="ui inverted vertical menu">
        <div class="item">
          <a class="ui logo icon image" href="/">
            <img width="40" height="40" src="https://i.pinimg.com/originals/16/aa/df/16aadf06716be2bb9c958b31ee1173a1.jpg">
          </a>
          <a href="/"><b>MC-Event</b></a>
        </div>
        <a class="item logout" href="{{ route('getLogoutAdmin') }}">
          <b>Logout</b>
        </a>
        <div class="item active">
          <div class="header">Danh sách API</div>
          <div class="menu">
            <a class="item" href="{{ url('admincp/api/user') }}">
              User
            </a>

            <a class="item" href="{{ url('admincp/api/event') }}">
              Event
            </a>
            <a class="item" href="{{ url('admincp/api/vote') }}">
              Vote
            </a>
            <a class="item" href="{{ url('admincp/api/opinion') }}">
              Opinion
            </a>
            <a class="item" href="{{ url('admincp/api/participant') }}">
              Participant
            </a>
            <a class="item" href="{{ url('admincp/api/date') }}">
              Date
            </a>
            <a class="item" href="{{ url('admincp/api/date_par') }}">
              Date_Par
            </a>
          </div>
        </div>
        <a class="item">
          Messages
        </a>
        <a class="item">
          Friends
        </a>
      </div>
    </div>
    
    <div class="admin-content">

      <!-- Nav Header -->
      <div class="ui menu">
        <a class="item">Browse</a>
        <a class="item">Submit</a>
        <div class="right menu logout">
          <a class="item" href="{{ route('getLogoutAdmin') }}">Logout</a>
        </div>
      </div>
      
      <!-- Content -->
      @yield('content')
    </div>

  </div>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('js/semantic.min.js') }}"></script>
  <script language='javascript'>
   $(document).ready(function(){
    $('.ui.accordion').accordion();
  });
</script>
</body>
</html>
