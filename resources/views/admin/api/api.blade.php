@extends('admin.layouts.app')

@section('title')
MC-Event Admin
@endsection

@section('content')
<div class="article">
  <div class="ui styled fluid accordion">

    @foreach ($datas as $key => $data)

      @if ($loop->first)

      <!-- Active -->
      <div class="active title">
        <i class="dropdown icon"></i>
        {{ $data['title'] }}
      </div>
      <div class="active content">
        <table class="ui celled table">
          <thead>
            <tr class="center aligned">
              <th class="api-name">Tên</th>
              <th class="api-content">Thông số</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($data['content'] as $key => $content)
            <tr>
              <td>{{ $key }}</td>
              <td>{!! $content !!}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      @else

      <!-- Not active -->
      <div class="title">
        <i class="dropdown icon"></i>
        {{ $data['title'] }}
      </div>
      <div class="content">
        <table class="ui celled table">
          <thead>
            <tr class="center aligned">
              <th class="api-name">Tên</th>
              <th class="api-content">Thông số</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($data['content'] as $key => $content)
            <tr>
              <td>{{ $key }}</td>
              <td>{{ $content }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      @endif
    @endforeach

  </div>
</div>
@endsection