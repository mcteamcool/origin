@extends('admin.layouts.app')

@section('title')
MC-Event Admin
@endsection

@section('content')
<div class="article">
  <div class="ui styled fluid accordion">

    <!-- Active -->
    <div class="active title">
      <i class="dropdown icon"></i>
      SHOW Event
    </div>
    <div class="active content">
      <table class="ui celled table">
        <thead>
          <tr class="center aligned">
            <th class="api-name">Tên</th>
            <th class="api-content">Thông số</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Link</td>
            <td>{{ config('app.apiUrl') . 'event/{id}' }}</td>
          </tr>
          <tr>
            <td>Method</td>
            <td>GET</td>
          </tr>
          <tr>
            <td>Response</td>
            <td>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <!-- Not active -->
    <div class="title">
      <i class="dropdown icon"></i>
      CREATE Event
    </div>
    <div class="content">
      <table class="ui celled table">
        <thead>
          <tr class="center aligned">
            <th class="api-name">Tên</th>
            <th class="api-content">Thông số</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Link</td>
            <td>{{ config('app.apiUrl') . 'event' }}</td>
          </tr>
          <tr>
            <td>Method</td>
            <td>POST</td>
          </tr>
          <tr>
            <td>Request</td>
            <td>
              - key: e932340d4304d64b91f4<br>
              - name: varchar(100) NOTNULL <br>
              - note: varchar(225) <br>
              - canCreateVoteFromOthers: boolean NOTNULL<br>
            </td>
          </tr>
          <tr>
            <td>Response</td>
            <td>
              {<br>
                &emsp;&emsp;"status": true,<br>
                &emsp;&emsp;"msg": "Create event successfully",<br>
                &emsp;&emsp;"link": "4327f6bc34190ecac4a555b02a1ba9c7"<br>
              }<br><br>
              Or:<br><br>
              {<br>
                &emsp;&emsp;"status": "Failed",<br>
                &emsp;&emsp;"error": {<br>
                &emsp;&emsp;&emsp;&emsp;"errorInfo": [
                "23000",
                1048,
                "Column 'name' cannot be null"
                ]<br>
                &emsp;&emsp;}<br>
              }
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <!-- Not active -->
    <div class="title">
      <i class="dropdown icon"></i>
      UPDATE Event
    </div>
    <div class="content">
      <table class="ui celled table">
        <thead>
          <tr class="center aligned">
            <th class="api-name">Tên</th>
            <th class="api-content">Thông số</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Link</td>
            <td>{{ config('app.apiUrl') . 'event/{id}' }}</td>
          </tr>
          <tr>
            <td>Method</td>
            <td>PUT</td>
          </tr>
          <tr>
            <td>Params</td>
            <td>
              - key: e932340d4304d64b91f4<br>
              - name: varchar(100) NOTNULL <br>
              - note: varchar(225) <br>
              - canCreateVoteFromOthers: boolean NOTNULL<br>
            </td>
          </tr>
          <tr>
            <td>Response</td>
            <td>
              {<br>
                &emsp;&emsp;"status": true,<br>
                &emsp;&emsp;"msg": "Update event successfully"<br>
              }<br><br>
              Or:<br><br>
              {<br>
                &emsp;&emsp;"status": "Failed",<br>
                &emsp;&emsp;"error": {<br>
                &emsp;&emsp;&emsp;&emsp;"errorInfo": [
                "23000",
                1048,
                "Column 'name' cannot be null"
                ]<br>
                &emsp;&emsp;}<br>
              }
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection