@extends('admin.layouts.app')

@section('title')
MC-Event Admin
@endsection

@section('content')
<div class="article">
  <div class="ui styled fluid accordion">

    <!-- Active -->
    <div class="active title">
      <i class="dropdown icon"></i>
      CREATE Opinion
    </div>
    <div class="active content">
      <table class="ui celled table">
        <thead>
          <tr class="center aligned">
            <th class="api-name">Tên</th>
            <th class="api-content">Thông số</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Link</td>
            <td>{{ config('app.apiUrl') . 'opinion/' }}</td>
          </tr>
          <tr>
            <td>Method</td>
            <td>POST</td>
          </tr>
          <tr>
            <td>Request</td>
            <td>
              - key: e932340d4304d64b91f4<br>
              - name: varchar(100) NOTNULL <br>
              - vote_id: int NOTNULL <br>
            </td>
          </tr>
          <tr>
            <td>Response</td>
            <td>
              {<br>
                &emsp;&emsp;"status": true,<br>
                &emsp;&emsp;"msg": "Create opinion successfully",<br>
              }<br><br>
              Or:<br><br>
              {<br>
                &emsp;&emsp;"status": "Failed",<br>
                &emsp;&emsp;"error": {<br>
                &emsp;&emsp;&emsp;&emsp;"errorInfo": [
                "23000",
                1048,
                "Column 'name' cannot be null"
                ]<br>
                &emsp;&emsp;}<br>
              }
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <!-- Not active -->
    <div class="title">
      <i class="dropdown icon"></i>
      UPDATE Vote
    </div>
    <div class="content">
      <table class="ui celled table">
        <thead>
          <tr class="center aligned">
            <th class="api-name">Tên</th>
            <th class="api-content">Thông số</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Link</td>
            <td>{{ config('app.apiUrl') . 'opinion/{id}' }}</td>
          </tr>
          <tr>
            <td>Method</td>
            <td>PUT</td>
          </tr>
          <tr>
            <td>Params</td>
            <td>
              - key: e932340d4304d64b91f4<br>
              - name: varchar(100) NOTNULL <br>
              - voted: int NOTNULL <br>
            </td>
          </tr>
          <tr>
            <td>Response</td>
            <td>
              {<br>
                &emsp;&emsp;"status": true,<br>
                &emsp;&emsp;"msg": "Update opinion successfully"<br>
              }<br><br>
              Or:<br><br>
              {<br>
                &emsp;&emsp;"status": "Failed",<br>
                &emsp;&emsp;"error": {<br>
                &emsp;&emsp;&emsp;&emsp;"errorInfo": [
                "23000",
                1048,
                "Column 'name' cannot be null"
                ]<br>
                &emsp;&emsp;}<br>
              }
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection