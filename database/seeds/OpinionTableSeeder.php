<?php

use Illuminate\Database\Seeder;

class OpinionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 25; $i++)
        {
            DB::table('opinion')->insert([
                'name' => 'Opinion' . $i,
                'voted' => 4,
                'vote_id' => $i,
            ]);
        }
    }
}
