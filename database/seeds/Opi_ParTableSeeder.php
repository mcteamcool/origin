<?php

use Illuminate\Database\Seeder;

class Opi_ParTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 25; $i++)
        {
            DB::table('opi_par')->insert([
                'opinion_id' => $i,
                'participant_id' => $i,
            ]);
        }
    }
}