<?php

use Illuminate\Database\Seeder;

class ParticipantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 25; $i++)
        {
            DB::table('participant')->insert([
                'name' => 'Participant ' . $i,
                'ipaddress' => 'ipaddress ' . $i,
                'event_id' => $i
            ]);
        }
    }
}
