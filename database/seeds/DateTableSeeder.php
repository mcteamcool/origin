<?php

use Illuminate\Database\Seeder;

class DateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 25; $i++)
        {
            DB::table('date')->insert([
                'name' => 'Date' . $i,
                'event_id' => $i,
            ]);
        }
    }
}
