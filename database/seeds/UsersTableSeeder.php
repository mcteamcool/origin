<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('1234'),
            'role' => 0
        ]);

        for ($i=1; $i < 25; $i++)
        {
            DB::table('users')->insert([
                'name' => 'user' . $i,
                'email' => 'guest'. $i .'@gmail.com',
                'password' => bcrypt('1234'),
                'role' => 1
            ]);
        }
    }
}
