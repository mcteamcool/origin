<?php

use Illuminate\Database\Seeder;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=1; $i < 25; $i++)
      {
          DB::table('event')->insert([
              'name' => 'Event' . $i,
              'note' => 'This is note of event',
              'link' => md5(microtime(true)),
              'canCreateVoteFromOthers' => true,
              // 'participant_id' => $i,
          ]);
      }
    }
}
