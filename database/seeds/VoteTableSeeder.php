<?php

use Illuminate\Database\Seeder;

class VoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 25; $i++)
        {
            DB::table('vote')->insert([
                'name' => 'Vote' . $i,
                'event_id' => $i
            ]);
        }
    }
}
