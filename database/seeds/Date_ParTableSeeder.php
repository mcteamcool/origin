<?php

use Illuminate\Database\Seeder;

class Date_ParTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 25; $i++)
        {
            DB::table('date_par')->insert([
                'id' => $i,
                'isGoing' => true,
                'isConsidering' => false,
                'comment' => 'Comment ' . $i,
                'date_id' => $i,
                'participant_id' => $i
            ]);
        }
    }
}
