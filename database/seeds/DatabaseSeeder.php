<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EventTableSeeder::class);
        $this->call(DateTableSeeder::class);
        $this->call(ParticipantTableSeeder::class);
        $this->call(VoteTableSeeder::class);
        $this->call(OpinionTableSeeder::class);
        $this->call(Date_ParTableSeeder::class);
        $this->call(Opi_ParTableSeeder::class);
    }
}