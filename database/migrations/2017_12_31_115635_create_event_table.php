<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('event', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 100);
      $table->string('note')->nullable();
      $table->string('link', 100);
      $table->boolean('canCreateVoteFromOthers')->default(true);
      $table->timestamps();

      // Foreign
      // $table->integer('user_id')->unsigned()->nullable();
      // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

      // $table->integer('participant_id')->unsigned()->nullable(false);
      // $table->foreign('participant_id')->references('id')->on('participant')->onDelete('cascade');
    });
  }

  /**
     * Reverse the migrations.
     *
     * @return void
     */
  public function down()
  {
    Schema::dropIfExists('event');
  }
}
