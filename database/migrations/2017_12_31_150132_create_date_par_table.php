<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDateParTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_par', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('isGoing');
            $table->boolean('isConsidering');
            $table->string('comment')->nullable();
            $table->timestamps();

            // Foreign
            $table->integer('date_id')->unsigned();
            $table->foreign('date_id')->references('id')->on('date')->onDelete('cascade');

            $table->integer('participant_id')->unsigned();
            $table->foreign('participant_id')->references('id')->on('participant')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('date_par');
    }
}
