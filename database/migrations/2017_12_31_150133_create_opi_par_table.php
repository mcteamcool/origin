<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpiParTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opi_par', function (Blueprint $table) {
            $table->timestamps();

            // Foreign
            $table->integer('opinion_id')->unsigned();
            $table->foreign('opinion_id')->references('id')->on('opinion')->onDelete('cascade');

            $table->integer('participant_id')->unsigned();
            $table->foreign('participant_id')->references('id')->on('participant')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opi_par');
    }
}
