let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/admin.scss', 'public/css/admin')
   .copy('node_modules/semantic-ui/dist/semantic.min.css','public/css/semantic.min.css')
   .copy('node_modules/semantic-ui/dist/semantic.min.js','public/js/semantic.min.js')
   .copy('node_modules/semantic-ui-calendar/dist/calendar.min.css','public/css/calendar.min.css')
   .copy('node_modules/semantic-ui-calendar/dist/calendar.min.js','public/js/calendar.min.js');
